# ChemoSoftware Group

Welcome to the group of software development and software availability (dev/avil) for Chemometrics. We are a group of researchers who are motivated to give Chemometrics a boost into open source software.  

## 🚀 Our motivation

1. **Chemometrics road to solid open source** 
   
    As research and technology progresses, we have come to sensibily notice how scattered open source solutions for modern Chemometrics are. Given the great advantages of open source software, such as accessibility and capacity to audit, we consider this to be a great oportunity to grow as a research community.   
   
2.  **Bringing the pieces together**

    A great deal of recent research has started to contribute to the Chemometrics field releasing packages or independent source code containing the methodology developed/use in academic published research. Such releases nonetheless live scattered. In the view of solidifying the availability of methods, publishing our own software under the hub of a Chemometrics community would benefit all parties. While our individual work will always remain in due authorship, our impact for users increases significantly.  

3.  **Researchers community**

    A community intended for software dev/avil is not a staple for academic non-software developers nowadays. We believe this can make a difference for Chemometrics and inspire other fields. 

4. **Publishing in the new science era**

    As we continue developing our scientific work, releasing our methodology solutions into repositories from this community will lead us to a new paradigm of academic publications in the new science era. 

## 🧐 What's inside?

A quick look at the initial state of the group.

    .
    ├── Welcome
    ├── InPython
    ├── InR
    

1.  🏠**Welcome**: This is a repo. It contains information about this group project. What you are reading is hosted in it. 

2.  **InPython**: This is a subgroup within the ChemoSoftware group. This subgroup is intended to contain repositories of software dev/avil in Python for Chemometrics. Inside, there's initially one repository of the _pycaltransfer_ package. Repositories that live in this subgroup will have a url of the type _https://gitlab.com/chemsoftware/python/repo_ .

3. **InR**: This is a subgroup within the ChemoSoftware group. This subgroup is intended to contain repositories of software dev/avil in R for Chemometrics. Inside, there's initially one repository of the _rcaltransfer_ package. Repositories that live in this subgroup will have a url of the type _https://gitlab.com/chemsoftware/rproject/repo_ .


## ☕ Becoming a member

There are several types of members in Gitlab. The detailed information about this can be found [here](https://docs.gitlab.com/ee/user/permissions.html). We will elaborate more on this in the near future.

## 🎓 Doubts?

Contact our administration members.
valeria.fonseca.diaz@gmail.com
